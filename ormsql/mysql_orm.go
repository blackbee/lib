package ormsql

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"reflect"
	"fmt"
	"log"
)

type SQLSet struct {
	TypeDB   string
	Server   string
	Port     string
	Protocol string
	NameDB   string
	UserDB   string
	PassDB   string
}

type SQLObject struct{
	DB *sql.DB
}

func CountRows(db *SQLObject, table string, where string) (string, error) {
	var countRows string
	countSelect := "SELECT COUNT(*) FROM " + table
	if where != "" {
		countSelect = countSelect + " WHERE " + where
	}

	rows, err := db.DB.Query(countSelect)

	if err != nil {
		return "", err
	}
	for rows.Next() {
		err = rows.Scan(&countRows)
		if err != nil {
			return "", err
		}
	}
	return countRows, err
}

func New(config *SQLSet) (*SQLObject, error){
	db, err := sql.Open(config.TypeDB,
		config.UserDB+":"+config.PassDB+"@"+config.Protocol+"("+config.Server+":"+config.Port+")/"+config.NameDB+"?charset=utf8")
	if err != nil {
		return nil, err
	}
	return &SQLObject{
		db,
	}, nil
}

func (obj SQLObject) Close(){
	obj.DB.Close()
}

func (obj SQLObject) SelectToOne(value interface{}, from string, where string) error {

	endSimbol := ","
	selectString := "SELECT "

	t := reflect.ValueOf(value).Elem()
	lenStruct := t.NumField()

	valsString := make([]sql.NullString, lenStruct) // Allocate enough values
	valsInt := make([]sql.NullInt64, lenStruct)     // Allocate enough values
	valsFloat := make([]sql.NullFloat64, lenStruct) // Allocate enough values
	ints := make([]interface{}, lenStruct)          // Allocate middle values

	for npc := 0; npc < lenStruct; npc++ { // iterates through every struct type field
		tag := t.Type().Field(npc).Tag // returns the tag string
		if npc == lenStruct-1 {
			endSimbol = " FROM "
		}
		selectString = selectString + tag.Get("sql") + endSimbol

		typeOfValues := t.Type().Field(npc).Type.Name()

		switch typeOfValues {
		case "string":
			ints[npc] = &valsString[npc] // Copy references into the middle values slice
		case "int":
			ints[npc] = &valsInt[npc]
		case "float64":
			ints[npc] = &valsFloat[npc]
		default:
			fmt.Println("Error UNKNOWN type: ", typeOfValues)
		}

	}

	selectString = selectString  + from + " WHERE " + where

	fmt.Println(selectString)
	rows, err := obj.DB.Query(selectString)

	if err != nil {
		return err
	} else {
		for rows.Next() {
			err = rows.Scan(ints...)
			if err != nil {
				return err
			}
			for i := range ints {
				v:=t.Field(i)
				switch ints[i].(type) {
				case *sql.NullString:
					if valsString[i].Valid {
						v.SetString(valsString[i].String) // Copy not NULL values into variables of struct
					} else {
						v.SetString("")
					}
				case *sql.NullInt64:
					if valsInt[i].Valid {
						v.SetInt(valsInt[i].Int64)
					} else {
						v.SetInt(0)
					}
				case *sql.NullFloat64:
					if valsFloat[i].Valid {
						v.SetFloat(valsFloat[i].Float64)
					} else {
						v.SetFloat(0)
					}
				default:
					log.Printf("Error UNKNOWN type: %T", ints[i])
				}
			}
		}
	}
	return nil
}
func (obj SQLObject) SelectOneToMany(value interface{}, from string, where string) error {

	endSimbol := ","
	selectString := "SELECT "

	t := reflect.ValueOf(value).Elem()
	lenStruct := t.NumField()

	valsString := make([]sql.NullString, lenStruct) // Allocate enough values
	valsInt := make([]sql.NullInt64, lenStruct)     // Allocate enough values
	valsFloat := make([]sql.NullFloat64, lenStruct) // Allocate enough values
	ints := make([]interface{}, lenStruct)          // Allocate middle values

	for npc := 0; npc < lenStruct; npc++ { // iterates through every struct type field
		tag := t.Type().Field(npc).Tag // returns the tag string
		if npc == lenStruct-1 {
			endSimbol = " FROM "
		}
		selectString = selectString + tag.Get("sql") + endSimbol

		typeOfValues := t.Type().Field(npc).Type.Name()

		switch typeOfValues {
		case "string":
			ints[npc] = &valsString[npc] // Copy references into the middle values slice
		case "int":
			ints[npc] = &valsInt[npc]
		case "float64":
			ints[npc] = &valsFloat[npc]
		default:
			fmt.Println("Error UNKNOWN type: ", typeOfValues)
		}

	}

	selectString = selectString  + from + " WHERE " + where

	fmt.Println(selectString)
	rows, err := obj.DB.Query(selectString)

	if err != nil {
		fmt.Printf("Error read data error: %v", err)
		return err
	} else {
		for rows.Next() {
			err = rows.Scan(ints...)
			if err != nil {
				log.Println("Failed to scan row", err)
				return err
			}
			for i := range ints {
				v:=t.Field(i)
				switch ints[i].(type) {
				case *sql.NullString:
					if valsString[i].Valid {
						v.SetString(valsString[i].String) // Copy not NULL values into variables of struct
					} else {
						v.SetString("")
					}
				case *sql.NullInt64:
					if valsInt[i].Valid {
						v.SetInt(valsInt[i].Int64)
					} else {
						v.SetInt(0)
					}
				case *sql.NullFloat64:
					if valsFloat[i].Valid {
						v.SetFloat(valsFloat[i].Float64)
					} else {
						v.SetFloat(0)
					}
				default:
					log.Printf("Error UNKNOWN type: %T", ints[i])
				}
			}
		}
	}
	fmt.Println(value)
	return nil
}
func (obj SQLObject) SelectToMany(value interface{}, from string, where string, valueArr []interface{}) (interface{},error) {
	endSimbol := ","
	selectString := "SELECT "



	t := reflect.ValueOf(value).Elem()

	lenStruct := t.NumField()

	valsString := make([]sql.NullString, lenStruct) // Allocate enough values
	valsInt := make([]sql.NullInt64, lenStruct)     // Allocate enough values
	valsFloat := make([]sql.NullFloat64, lenStruct) // Allocate enough values
	ints := make([]interface{}, lenStruct)          // Allocate middle values

	for npc := 0; npc < lenStruct; npc++ { // iterates through every struct type field
		tag := t.Type().Field(npc).Tag // returns the tag string
		if npc == lenStruct-1 {
			endSimbol = " FROM "
		}
		selectString = selectString + tag.Get("sql") + endSimbol

		typeOfValues := t.Type().Field(npc).Type.Name()

		switch typeOfValues {
		case "string":
			ints[npc] = &valsString[npc] // Copy references into the middle values slice
		case "int":
			ints[npc] = &valsInt[npc]
		case "float64":
			ints[npc] = &valsFloat[npc]
		default:
			fmt.Println("Error UNKNOWN type: ", typeOfValues)
		}

	}

	selectString = selectString  + from + " WHERE " + where

	fmt.Println(selectString)
	rows, err := obj.DB.Query(selectString)

	if err != nil {
		fmt.Printf("Error read data error: %v", err)
		return nil,err
	} else {
		for rows.Next() {
			err = rows.Scan(ints...)
			if err != nil {
				log.Println("Failed to scan row", err)
				return nil,err
			}
			for i := range ints {
				v:=t.Field(i)
				switch ints[i].(type) {
				case *sql.NullString:
					if valsString[i].Valid {
						v.SetString(valsString[i].String) // Copy not NULL values into variables of struct
					} else {
						v.SetString("")
					}
				case *sql.NullInt64:
					if valsInt[i].Valid {
						v.SetInt(valsInt[i].Int64)
					} else {
						v.SetInt(0)
					}
				case *sql.NullFloat64:
					if valsFloat[i].Valid {
						v.SetFloat(valsFloat[i].Float64)
					} else {
						v.SetFloat(0)
					}
				default:
					log.Printf("Error UNKNOWN type: %T", ints[i])
				}
			}

			fmt.Println(ints)

			valueArr = append(valueArr,t.Interface())

		}
	}
	fmt.Println("-->",valueArr)
	return valueArr,nil
}
